<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
  Template name: Skabelon: Forside
 * @package morten
 */
get_header();
?>


<div id="forside">
    <!-- Henter slider her -->
    <div>
        <?php get_template_part('inc/sektioner/slider', get_post_format()); ?>
    </div>

    <section class="arkiv_indhold">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-7 artikler">
                    <h2 class="padall25">Nyeste historier</h2>

                    <?php
                    $args = array(
                        'posts_per_page' => 10,
                        'paged' => $paged,
                        'post_type' => array( 'youtube', 'post' )
                        );
                    $loop = new WP_Query( $args );
                    if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();
                        include(locate_template('inc/sektioner/arkiv-video.php', get_post_format()));
                    endwhile; endif; ?>
                    <a href="wordpress/videoer">
                    <div class="se_alle">
                    <h3>Se alle videoer</h3>
                    </div>
                    </a>
                </div>
                <div class="col-1"></div>
                <div class="col-12 col-lg-4 podcasts">
                    <h2 class="padall25">Nyeste podcasts</h2>

                    <?php
                    $args = array(
                        'post_type' => 'soundcloud',
                        'posts_per_page' => 10,
                        'paged' => $paged
                        );
                    $loop = new WP_Query( $args );
                    if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();
                        include(locate_template('inc/sektioner/arkiv-podcast.php', get_post_format()));
                    endwhile; endif; ?>
                    <a href="wordpress/podcast">
                    <div class="se_alle">
                    <h3>Se alle podcasts</h3>
                </div>
            </div>
        </div>
    </section>
</div>


<?php get_footer(); ?>