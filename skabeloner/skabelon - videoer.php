<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
  Template name: Skabelon: Videoer
 * @package morten
 */
get_header();
?>


<div id="videoer">
    <!-- Henter subheader-slider her -->
    <div>
        <?php get_template_part('inc/sektioner/subheader', get_post_format()); ?>
    </div>

    <section class="arkiv_indhold">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 nospace_mobil">
                    <div class="artikler">



<!-- Ser om URl indeholder ordet "XXX", hvis den gør, sættet post_kategori til "XXX" -->
                    <?php 
                    if(strpos($_SERVER['REQUEST_URI'], 'videoer/programmet') !== false){
                        $post_kategori = 'programmet'; 
                        
                    } else if (strpos($_SERVER['REQUEST_URI'], 'videoer/hurtig-snas') !== false){
                        $post_kategori = 'hurtig_snas';

                    } else if (strpos($_SERVER['REQUEST_URI'], 'videoer/news') !== false){
                        $post_kategori = 'news';

                    } else if (strpos($_SERVER['REQUEST_URI'], 'videoer/retro') !== false){
                        $post_kategori = 'retro';

                    } else if (strpos($_SERVER['REQUEST_URI'], 'videoer/andet') !== false){
                        $post_kategori = 'andet';

                    } else if (strpos($_SERVER['REQUEST_URI'], 'videoer/serier') !== false){
                        $post_kategori = 'serier';

                    } else if (strpos($_SERVER['REQUEST_URI'], 'videoer/anmeldelser') !== false){
                        $post_kategori = 'anmeldelse';

                    } else if (strpos($_SERVER['REQUEST_URI'], 'videoer/programmet') !== false){
                        $post_kategori = 'programmet';

                    } else {
                        $post_kategori = '';
                        echo $post_kategori;
                    }
                    



                    

                     ?>
                        <h2 class="padall25">Nyeste videoer</h2>
                        <?php
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
                        $args = array(
                                'post_type' => 'youtube',
                                'posts_per_page' => 10,
                                'kategori' => $post_kategori,
                                'paged' => $paged
                                );
                        $loop = new WP_Query( $args );
                        if ( $loop->have_posts()) : while ( $loop->have_posts()) : $loop->the_post();
                            include(locate_template('inc/sektioner/arkiv-video.php', get_post_format()));
                        endwhile;
                        
                        include(locate_template('inc/sektioner/post-nav.php', get_post_format()));
                        
                       endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php get_footer(); ?>