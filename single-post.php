<?php


get_header(); ?>


<div>
        <?php get_template_part('inc/sektioner/subheader-slider', get_post_format()); ?>
</div>

<div id="single_blog">
<section class="video">
    <div class="container">
        <div class="row">    
            

                            <!-- Henter titlen -->
                            <div class="col-xl-12 margin50 padall50 beskrivelse">
                                <h3>
                                <?php the_title(); ?>
                                </h3>

                                <h6><?php the_field('underoverskrift'); ?></h6>

                                <!-- Henter dato -->                        
                                <p class="dato">
                                        <?php echo get_the_date(); ?>
                                    </p>

                                <!-- Henter beskrivelse -->
                                <p>
                                <?php $summary = get_field('brodtext');
                                    echo $summary;   
                                ?>
                                </p>
                                <div class="col-xl-12 d-flex flex-column align-items-end">
                                            <div class="social_share">
                                                <p>Del artiklen</p>
                                            </div>
                                            <div class="social_share_a">
                                                <a href="http://www.facebook.com/sharer.php?u=<?php echo get_permalink();?> " target="_blank"><img src="https://rasmusra.dk/wordpress/filer/facebook.png" alt=""></a>
                                                <a href="http://twitter.com/share?url=<?php echo get_permalink();?>&text=<?php echo get_the_title(); ?>" target="_blank"><img src="https://rasmusra.dk/wordpress/filer/twitter.png" alt=""></a>
                                                <a href="http://reddit.com/submit?url=<?php echo get_permalink();?>&title=<?php echo get_the_title(); ?>" target="_blank"><img src="https://rasmusra.dk/wordpress/filer/reddit.png" alt=""></a>
                                            </div>
                                            </div>

                                </div>
                                </div>

                                <div class="row margin25 padall50 video_fakta">

                                    <!-- Henter medarbejder -->
                                            <?php
                                            $posts = get_field('tilknyttet_journalist');
                                            if( $posts):
                                                foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
                                                    
                                                        <div class="medarbejder col-xl-2">
                                                        <a href="<?php echo get_permalink($p);?>" > 
                                                                <?php $image = get_field('billede', $p->ID, true);
                                                                if(!empty($image) ): 
                                                                    $rImage = aq_resize($image['url'], 150, 150, true, true, true);
                                                                    
                                                                
                                                                if($rImage) {
                                                                    echo'<img class="img-fluid" src="' . $rImage . '" alt="' . get_the_title() . '" />';}
                                                                ?>
                                                                
                                                                <?php endif; ?>

                                                                <p>
                                                                    <?php the_field('kort_navn', $p->ID); ?>
                                                                </p>
                                                        <a/>
                                                        </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                    
                                    </section>
                                </div>

                                <!-- Facebook kommentarer starter --> 
                                <?php include(locate_template('inc/sektioner/facebook-comment.php', get_post_format()));?>

					
					


            
            
        



    <?php get_footer(); ?>