<a href="<?php echo get_permalink(); ?>">
    <article class="padding25" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4">
                    <?php $image = get_field('blog_billede');

                        if (!empty($image)) {
                        $rImage = aq_resize($image, 300, 200, true, true, true);}
                        ?>

                        <?php
                            if ($rImage) {
                                echo'<img class="img-fluid" src="' . $rImage . '" alt="' . get_the_title() . '" />';
                            }
                        ?>    
                </div>
                <div class="col-xl-8">
                    <h3> <?php the_title(); ?></h3>
                    <h4><?php the_field('underoverskrift'); ?></h4>
                    
                    <p class="dato">
                        <?php echo get_the_date(); ?>
                    </p>
                    <p>
                        <?php $summary = get_field('brodtext');
                        $summary = substr($summary, 0, 125);
                        $summary = $summary .    '...';
                        echo $summary;   
                        ?>
                    </p>

                    <div class="d-flex dato_medarbejder">
                        <p>Af</p>
                
                        <?php 
                        $posts = get_field('tilknyttet_journalist');
                        if( $posts): ?>
                            <?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
                                <p class="medarbejder"><?php the_field('kort_navn', $p->ID); ?>, </p>
                            <?php endforeach; endif; ?> 
                    </div>
                </div>
            </div>
        </div>
    </article>
</a>



