<a href="<?php echo get_permalink(); ?>">
    <div class="col-xl-12 nospace">
        <article class="padding25" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="container-fluid">
                <div class="row">



                    <?php 
                    if(strpos($_SERVER['REQUEST_URI'], '/podcast') !== false){ ?>
                        <div class="col-12 col-md-3">
                        <img class="playIkon" src="https://rasmusra.dk/wordpress/filer/podcast_ikon.png" alt="">
                        <img src="<?php the_field('tn_link'); ?>" alt="">    
                    </div>
                    <div class="col-12 col-md-9">
                    <?php     
                    } else{ ?>
                        <div class="col-12 col-md-5">
                        <img class="playIkon" src="https://rasmusra.dk/wordpress/filer/podcast_ikon.png" alt="">
                        <img src="<?php the_field('tn_link'); ?>" alt="">    
                    </div>
                    <div class="col-12 col-md-7">
                    <?php     
                    } 
                    
                    ?>
                    
                    
                        <h3>
                            <?php the_title(); ?>
                        </h3>
                        <p class="dato">
                                <?php echo get_the_date(); ?>
                            </p>
                    

                    </div>
                </div>
            </div>
        </article>
    </div>
</a>



