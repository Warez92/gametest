<div class="forside-slider">

    <?php 
                $args = array( 'posts_per_page' => 5, 'post_type' => 'slider', 'order' => 'ASC',);
                $loop = new WP_Query( $args );
                while ( $loop->have_posts() ) : $loop->the_post(); 
           ?>

<?php
    $thumb = get_post_thumbnail_id();
    if ($thumb) {
        $img_url = wp_get_attachment_url($thumb, 'full'); //get full URL to image (use "large" or "medium" if the images too big)
    }
    
    $image = aq_resize($img_url, 1920, 400, true, true, true); //resize & crop the image

    if ($image) { ?>


    
<div>
<!-- XL -->
    <div class="sub-image">
        <img class="hidden-lg-down" src="<?php echo $image ?>" alt="<?php the_title(); ?>" />
    </div>

    <?php
        $image = aq_resize($img_url, 1140, 400, true, true, true); //resize & crop the image
    ?>
<!-- LG -->
    <div class="sub-image">
        <img class="hidden-md-down hidden-xl-up" src="<?php echo $image ?>" alt="<?php the_title(); ?>" />
    </div>

    <?php
        $image = aq_resize($img_url, 960, 400, true, true, true); //resize & crop the image
    ?>

<!-- MD -->
    <div class="sub-image">
        <img class="hidden-sm-down hidden-lg-up" src="<?php echo $image ?>" alt="<?php the_title(); ?>" />
    </div>

    <?php
        $image = aq_resize($img_url, 720, 400, true, true, true); //resize & crop the image
    ?>

<!-- SM -->
    <div class="sub-image">
        <img class="hidden-xs-down hidden-md-up" src="<?php echo $image ?>" alt="<?php the_title(); ?>" />
    </div>	

    <?php
        $image = aq_resize($img_url, 425, 400, true, true, true); //resize & crop the image
    ?>  

<!-- XS -->
    <div class="sub-image">
        <img class="hidden-sm-up" src="<?php echo $image ?>" alt="<?php the_title(); ?>" />
    </div>
    <?php } ?>	

        <div class="overlay <?php the_field('color'); ?>"> </div>

        
        <div class="subheaderText">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">

							<h6><?php the_field('overskrift'); ?></h6>

                            <h2>
                                <?php the_title(); ?>
                            </h2>

                            <?php the_content(); ?>


                            <?php if (get_field ('link_1') ) : ?>
                            <a href="<?php echo the_field('link_1'); ?>" class="cta_btn" tabindex="0">
                                <?php echo the_field('link_1_text'); ?>
                            </a>

                            <?php endif; ?>

                            <?php if (get_field ('link_2') ) : ?>
                            <a href="<?php echo the_field('link_2'); ?>" class="cta_btn cta_btn_2" tabindex="0">
                                <?php echo the_field('link_2_text'); ?>
                            </a>

                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div>
    <?php 
			endwhile;
		    wp_reset_postdata();
		?>

</div>

