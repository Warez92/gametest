<nav class="social hidden-md-dowm">
    <ul>
        <li class="twitter-bgcolor"><a href="https://twitter.com/GametestDanmark">Følg os på Twitter <i class="twitter-color fa fa-twitter"></i></a></li>
        <li class="facebook-bgcolor"><a href="https://www.facebook.com/GametestDanmark/">Følg os på Facebook <i class="facebook-color fa fa-facebook"></i></a></li>
        <li class="youtube-bgcolor"><a href="https://www.youtube.com/user/GametestDanmark">Følg os på Youtube <i class="youtube-color fa fa-youtube"></i></a></li>
    </ul>
</nav>


<nav class="socialMobil hidden-lg-up">
    <ul>
        <li class="twitter-bgcolor"><a href="https://twitter.com/GametestDanmark"><i class="twitter-color fa fa-twitter"></i></a></li>
        <li class="facebook-bgcolor"><a href="https://www.facebook.com/GametestDanmark/"><i class="facebook-color fa fa-facebook"></i></a></li>
        <li class="youtube-bgcolor"><a href="https://www.youtube.com/user/GametestDanmark"><i class="youtube-color fa fa-youtube"></i></a></li>
    </ul>
</nav>