<a href="<?php echo get_permalink(); ?>">
    <div class="col-xl-12 nospace">
        <article class="padding25" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-md-4">


                    <?php

                        if( get_post_type() == 'youtube' ) {?>
                            <img class="playIkon" src="https://rasmusra.dk/wordpress/filer/play_ikon.png" alt="">
                        <?php } ?>
                        
                        <img src="<?php the_field('tn_link'); ?>" alt="">    
                    </div>
                    <div class="col-12 col-md-8">
                        <h3>
                            <?php the_title(); ?>
                        </h3>
                        <p class="dato">
                                <?php echo get_the_date(); ?>
                            </p>
                            
                        <p>
                            <?php
                            $summary = get_field('beskrivelse');

                            if ($summary ==! false) {
                                $summary = substr($summary, 0, 100);
                                $summary = $summary .    '...';
                                echo $summary;   
                            } else { 
                                echo ' ';
                            }
                            ?>
                        
                        </p>

                        <div class="d-flex dato_medarbejder">
                            <p>Af</p>
                    
                            <?php 
                            $posts = get_field('tilknyttet_journalist');
                            if( $posts): ?>
                                <?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
                                    <p class="medarbejder"><?php the_field('kort_navn', $p->ID); ?><span>,</span> </p>
                                <?php endforeach; endif; ?>


                           
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</a>