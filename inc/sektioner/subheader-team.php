<section class="subheader">
<div class="overlay <?php the_field('color'); ?>"> </div>



    <?php
    $thumb = get_post_thumbnail_id();
    if ($thumb) {
        $img_url = wp_get_attachment_url($thumb, 'full'); //get full URL to image (use "large" or "medium" if the images too big)
    }
    
    $image = aq_resize($img_url, 1920, 400, true, true, true); //resize & crop the image

    if ($image) { ?>


    

<!-- XL -->
    <div class="sub-image">
        <img class="hidden-lg-down" src="<?php echo $image ?>" alt="<?php the_title(); ?>" />
    </div>

    <?php
        $image = aq_resize($img_url, 1140, 400, true, true, true); //resize & crop the image
    ?>
<!-- LG -->
    <div class="sub-image">
        <img class="hidden-md-down hidden-xl-up" src="<?php echo $image ?>" alt="<?php the_title(); ?>" />
    </div>

    <?php
        $image = aq_resize($img_url, 960, 400, true, true, true); //resize & crop the image
    ?>

<!-- MD -->
    <div class="sub-image">
        <img class="hidden-sm-down hidden-lg-up" src="<?php echo $image ?>" alt="<?php the_title(); ?>" />
    </div>

    <?php
        $image = aq_resize($img_url, 720, 400, true, true, true); //resize & crop the image
    ?>

<!-- SM -->
    <div class="sub-image">
        <img class="hidden-xs-down hidden-md-up" src="<?php echo $image ?>" alt="<?php the_title(); ?>" />
    </div>	

    <?php
        $image = aq_resize($img_url, 425, 400, true, true, true); //resize & crop the image
    ?>  

<!-- XS -->
    <div class="sub-image">
        <img class="hidden-sm-up" src="<?php echo $image ?>" alt="<?php the_title(); ?>" />
    </div>
    <?php } ?>	
 




    

    <div class="subheaderText">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                            <h2 class="citat">"<?php the_field('citat'); ?>"</h2>
                        </div>
                    </div>
                </div>
            </div>
    </section>

