<?php


get_header(); ?>

<div id="single_video">
<section class="video">
    <div class="container">
        <div class="row">
            
                            <!-- Henter video -->
                            <div class="col-xl-12 nospace video">
                                <?php the_field('video'); ?>
                            </div>
                            <!-- Henter titlen -->
                            <div class="col-12 col-xl-9 margin50 padall50 beskrivelse">
                                <h3>
                                    <?php the_title(); ?>
                                </h3>

                                <!-- Henter dato -->                        
                                <p class="dato">
                                        <?php echo get_the_date(); ?>
                                    </p>

                                <!-- Henter beskrivelse -->
                                <p>
                                <?php $summary = get_field('beskrivelse');
                                    echo $summary;   
                                ?>
                                </p>

                                </div>


                                <div class="col-12 col-xl-3 margin50 padall50 d-flex flex-column align-items-end video_fakta">
                                    <!-- Henter medarbejder -->
                                    
                                        <div class="row flex-row-reverse">

                                            <?php
                                            $posts = get_field('tilknyttet_journalist');
                                            if( $posts):
                                                foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
                                                    
                                                        <div class="medarbejder col-6">
                                                        <a href="<?php echo get_permalink($p);?>"> 
                                                                <?php $image = get_field('billede', $p->ID, true);
                                                                if( !empty($image) ): 
                                                                    $rImage = aq_resize($image['url'], 150, 150, true, true, true);
                                                                
                                                                if ($rImage) {
                                                                    echo'<img class="img-fluid" src="' . $rImage . '" alt="' . get_the_title() . '" />';}
                                                                ?>
                                                                
                                                                <?php endif; ?>

                                                                <p>
                                                                    <?php the_field('kort_navn', $p->ID); ?>
                                                                </p>
                                                           <a/>
                                                        </div>
                                                        
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>





      
                                    

                                    <div class="kategori">
                                        <p>Kategori</p>
                                        <?php echo get_the_term_list( $post->ID, 'kategori') ?>
                                    </div>


                                    <div class="social_share">
                                                <p>Del artiklen</p>
                                            </div>
                                            <div class="social_share_a">
                                                <a href="http://www.facebook.com/sharer.php?u=<?php echo get_permalink();?> " target="_blank"><img src="https://rasmusra.dk/wordpress/filer/facebook.png" alt=""></a>
                                                <a href="http://twitter.com/share?url=<?php echo get_permalink();?>&text=<?php echo get_the_title(); ?>" target="_blank"><img src="https://rasmusra.dk/wordpress/filer/twitter.png" alt=""></a>
                                                <a href="http://reddit.com/submit?url=<?php echo get_permalink();?>&title=<?php echo get_the_title(); ?>" target="_blank"><img src="https://rasmusra.dk/wordpress/filer/reddit.png" alt=""></a>
                                            </div>
                                    </section>
                                </div>

                                <!-- Facebook kommentarer starter --> 
                                <?php include(locate_template('inc/sektioner/facebook-comment.php', get_post_format()));?>

					
					


            
            
        



    <?php get_footer(); ?>