<?php
get_header();
global $wp_query;
?>
<section class="søgeresultater ">
<div class="container">
  <div class="row">
    <div class="col-xl-12">
      <h2> <?php echo $wp_query->found_posts; ?>
        <?php _e( 'Søgeresultater fundet for', 'locale' ); ?>: "<?php the_search_query(); ?>" </h2>

        <?php

        if (have_posts() && strlen( trim(get_search_query()) ) != 0 ) : while (have_posts()) : the_post(); ?>
         <div class="col-xl-12 d-flex justify-content-center">
              <a href="<?php the_permalink();?>">
        <h4><?php the_title();?></h4>
        <img src="<?php the_field('tn_link'); ?>" alt=""> 
         
        
        <p>
          <?php $summary = get_field('beskrivelse');
              $summary = substr($summary, 0, 150);
              $summary = $summary .    '...';
              echo $summary;   
          ?>
          </p>
          </a>
          </div>
        <hr>
         
         
        <?php endwhile;  ?>
        <?php echo paginate_links(); ?>
        <?php else:?>
         
         
        <h2>Ingen resultater fundet.</h2>
         
         
        <?php endif; ?>

        </div>
        </div>
        </div>
        </section>

        <?php get_footer(); ?>