<html>
<head>
<meta property="fb:app_id" content="937540103088426">
<!-- <meta property="og:description" content="Gametest FB comment" /> -->
<title><?php echo get_bloginfo( 'name' ); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php wp_head(); ?>
</head>
<body>


<script>
(function(d, s, id){
				 var js, fjs = d.getElementsByTagName(s)[0];
				 if (d.getElementById(id)) {return;}
				 js = d.createElement(s); js.id = id;
				 js.src = "//connect.facebook.net/da_DK/sdk.js#xfbml=1&version=v2.6";
				 fjs.parentNode.insertBefore(js, fjs);
               }(document, 'script', 'facebook-jssdk'));
</script>

<!--
<div class="loader">
         <div class="cs-loader">
  <div class="cs-loader-inner">
    <label>	●</label>
    <label>	●</label>
    <label>	●</label>
    <label>	●</label>
    <label>	●</label>
    <label>	●</label>
  </div>
</div>
        </div>

-->


<div class="mobilmenu-ms bg wrapper-menu hidden-lg-up">									  
    <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'ms_menu')); ?>
    <?php get_search_form(); ?>
</div>



<header id="header">

    <div class="container-fluid">
        <div class="row d-flex align-items-center">
            <div class="col-8 col-lg-3 col-xl-4">
                <div id="logo">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"> <img src="https://rasmusra.dk/wordpress/filer/GT_logo.png" alt="<?php bloginfo(); ?>" /> </a>
                </div>
            </div>
            

            <div class="mobilmenu col-4 hidden-lg-up"> 
                <div class="hamburger right mobil-hamburger" id="hamburger-4">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>
            </div>

            <div class="hidden-md-down col-lg-7 col-xl-5">
                <div class="main_menu">
                    <?php wp_nav_menu( array( 'theme_location' => 'primary' ,'menu_id' =>'') ); ?>
                </div>
            </div>
            
            <div class="hidden-md-down col-12 col-lg-2 col-xl-3">
            <?php get_search_form(); ?>
            </div>

            
        </div>
    </div>
    <?php get_template_part('inc/sektioner/social', get_post_format()); ?>


</header>