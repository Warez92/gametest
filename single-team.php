<?php


get_header(); ?>

<!-- Henter subheader-slider her -->
<?php get_template_part('inc/sektioner/subheader-team', get_post_format()); ?>

<div id="single_team">
    <section class="team_info sektion">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center"><?php 
                    $image = get_field('billede');

                    if (!empty($image)) {
                    $rImage = aq_resize($image['url'], 300, 300, true, true, true);}
                    ?>

                    <?php
                        if ($rImage) {
                            echo'<img class="img-fluid" src="' . $rImage . '" alt="' . get_the_title() . '" />';
                        }
                    ?>
                    <h1><?php the_field('fulde_navn'); ?></h1>
                    <h4><?php the_field('stilling'); ?></h4>
                </div>

                <div class="col-xl-12 nospace_mobil">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </section>

    <section class="team_4_hurtige sektion">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                        <h2><?php the_field('kort_navn'); ?>'s yndlingsting</h2>
                </div>
            </div>


            <div class="row">
                <div class="col-12 col-md-6 d-flex padall25 align-items-center">
                    <figure><img src="https://rasmusra.dk/wordpress/filer/platform_ikon.png" alt="<?php bloginfo(); ?>"></figure>
                    <div class="text">
                        <h4><?php the_field('1_overskrift'); ?></h4>
                        <p><?php the_field('1_text'); ?></p>
                    </div>
                </div>

                <div class="col-12 col-md-6 d-flex padall25 align-items-center">
                    <figure><img src="https://rasmusra.dk/wordpress/filer/genre_ikon.png" alt="<?php bloginfo(); ?>"></figure>
                    <div class="text">
                        <h4><?php the_field('2_overskrift'); ?></h4>
                        <p><?php the_field('2_text'); ?></p>                
                    </div>
                </div>

                <div class="col-12 col-md-6 d-flex padall25 align-items-center">
                    <figure><img src="https://rasmusra.dk/wordpress/filer/spil_ikon.png" alt="<?php bloginfo(); ?>"></figure>
                    <div class="text">
                        <h4><?php the_field('3_overskrift'); ?></h4>
                        <p><?php the_field('3_text'); ?></p>              
                    </div>
                </div>

                <div class="col-12 col-md-6 d-flex padall25 align-items-center">
                    <figure><img src="https://rasmusra.dk/wordpress/filer/karakter_ikon.png" alt="<?php bloginfo(); ?>"></figure>
                    <div class="text">
                        <h4><?php the_field('4_overskrift'); ?></h4>
                        <p><?php the_field('4_text'); ?></p>                
                    </div>
                </div>              
            </div>
        </div>
    </section>      
</div>


<?php include(locate_template('inc/sektioner/team.php', get_post_format()));?>


<?php get_footer(); ?>