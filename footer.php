<a href="#" class="scrollup" id="smoothup"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
<div id="footer">
    <div class="container">
        <div class="row"> 
            <div class="col-xl-12 text-center padding25">
               <p> Copyright &copy; <?php echo date('Y'); ?> <a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"> <?php bloginfo('name'); ?></a> - Alle rettigheder forbeholdes.</p>
            </div>  
        </div>
    </div>
</div>

</body>
</html>